#import libraries

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
import csv
import os
import requests
import json
import datetime

def get_job_info(job):
    try:
        #  # xuất dữ liệu JSON ra file
        # jobs_result = {}
        # print(job)
        job_payload = job.get('hostQueryExecutionResult', {}).get('data', {}).get('jobData', {}).get('results', [{}])[0].get('job', {})
        title = job_payload.get('title', None)   
        normalizedTitle = job_payload.get('normalizedTitle', '')
        company = job_payload.get('sourceEmployerName', '')
        datePublished = job_payload.get('datePublished', '')    
        description = job_payload.get('description', {}).get('text', '').replace("\n", r"\n") 
        benefits_list = job_payload.get('benefits', [])
        benefits = [benefit.get('label', None) for benefit in benefits_list]
        attributes_list = job_payload.get('attributes', [])
        attributes = [attribute.get('label', None) for attribute in attributes_list]

        employer = job_payload.get('employer', {})
        companyRating = employer.get('ugcStats', {}).get('ratings', {}).get('overallRating', {}).get('value', '')
        companyReviewCount = employer.get('ugcStats', {}).get('ratings', {}).get('overallRating', {}).get('count', '')

        salaryInfoModel = job.get('salaryInfoModel', '')
        if salaryInfoModel is not None:
            salaryMax = salaryInfoModel.get('salaryMax', "")
            salaryMin = salaryInfoModel.get('salaryMin', "")
            salaryType = salaryInfoModel.get('salaryType', "")
        else:
            salaryMax = ""
            salaryMin = ""
            salaryType = ""
        return {
            'title': title,
            'normalizedTitle': normalizedTitle,
            'company': company,
            'datePublished': datePublished,
            'description': description,
            'benefits': benefits,
            'attributes': attributes,
            'companyRating': companyRating,
            'companyReviewCount': companyReviewCount,
            'salaryMax': salaryMax,
            'salaryMin': salaryMin,
            'salaryType': salaryType,
        }
    except Exception as e:
        print("Error [get_job_info]:",e)

while True:
    root_path = '/home/xenwithu/Documents/Hoc/DUT/sm8/PBL/crawl_example_hihi/pbl/crawl_indeed_lasted'
    record_count = 0
    driver = webdriver.Chrome()
    browser = driver.get("https://www.indeed.com/") 
    current_job_crawling = ''
    q = ['python','sql','c++','c#','javascript','ruby','swift','php','html','css','angular', 'spring', 'scala', 'react', 'node', 'vue','.net']
    # q = ['java','python','sql','c++','c#','javascript','ruby','swift','php','html','css','angular', 'spring', 'scala', 'react', 'node', 'vue','.net']
    sc=['0kf%3Aexplvl%28MID_LEVEL%29%3B','0kf%3Aexplvl%28SENIOR_LEVEL%29%3B','0kf%3Aexplvl%28ENTRY_LEVEL%29%3B']
    for i in q:
        for j in sc: 
            mosaic_initial_data = None
            main_initial_data = None #get page number

            page = 0

            page_is_limit = False
            while True:
                try:
                    print("While repeat")
                    start = 0
                    if page == 0:
                        current_job_crawling = "https://www.indeed.com/jobs?q="+i+"&sc="+j
                        print('current_job_crawling:',current_job_crawling)
                    else:
                        start = (page)*10
                        current_job_crawling = "https://www.indeed.com/jobs?q="+i+"&sc="+j+"&start="+str(start)
                        print('current_job_crawling:',current_job_crawling)
                    # passing robot check
                    try:
                        driver.get(current_job_crawling)
                        mosaic_initial_data = driver.execute_script("return JSON.stringify(window.mosaic.initialData)")
                        main_initial_data = driver.execute_script("return JSON.stringify(window._initialData)")
                        # with open('./main_indeed.html', 'w') as file:
                        #     file.write(driver.page_source)
                    except Exception as e:
                        print("Error [passing robot check]:",e)
                        driver.quit()
                        driver = webdriver.Chrome()
                        driver.get(current_job_crawling)
                        wait = WebDriverWait(driver, 100)  
                        wait.until(EC.presence_of_all_elements_located((By.ID, 'mosaic-provider-jobcards')))
                        mosaic_initial_data = driver.execute_script("return JSON.stringify(window.mosaic.initialData)")
                        main_initial_data = driver.execute_script("return JSON.stringify(window._initialData)")
                    # check if page is limit
                    main_initial_data_json = json.loads(main_initial_data)
                    current_page = int(main_initial_data_json.get('pageNum'))
                    print('=================================================================================')
                    print('current_page:',current_page)
                    print('page:',page)
                    print('======================================================')
                    if(page < current_page):
                        page = current_page
                    else:
                        break

                    # get list link of job
                    initial_data_json = json.loads(mosaic_initial_data)
                    jobs = initial_data_json['publicMetadata']['mosaicProviderJobCardsModel']['results']

                    # get job info
                    for job in jobs:
                        try:
                            link_job = job.get('thirdPartyApplyUrl')
                            job_browser = driver.get(link_job)
                            _initial_data = driver.execute_script("return JSON.stringify(window._initialData)")
                            if _initial_data is not None:
                                _initial_data_json = json.loads(_initial_data)
                                # with open('./result_initial_data_4.json', 'w') as file:
                                #     file.write(_initial_data)
                                jobs_result = get_job_info(_initial_data_json)
                                # Kiểm tra xem tệp CSV đã có dòng tiêu đề hay chưa
                                csv_file_path = './indeed_jobs_2.csv'
                                file_exists = os.path.exists(csv_file_path)
                                if not file_exists or os.stat(csv_file_path).st_size == 0:
                                    with open(csv_file_path, mode='w', newline='', encoding='utf-8') as file:
                                        writer = csv.writer(file)
                                        writer.writerow("title,normalizedTitle,company,datePublished,description,benefits,attributes,companyRating,companyReviewCount,salaryMax,salaryMin,salaryType,jobUrl".split(","))
                                with open(csv_file_path, mode='a', newline='', encoding='utf-8') as file:
                                    writer = csv.writer(file)
                                    writer.writerow([str(jobs_result['title']),str(jobs_result['normalizedTitle']),str(jobs_result['company']),str(jobs_result['datePublished']),str(jobs_result['description']),str(jobs_result['benefits']),str(jobs_result['attributes']),str(jobs_result['companyRating']),str(jobs_result['companyReviewCount']),str(jobs_result['salaryMax']),str(jobs_result['salaryMin']),str(jobs_result['salaryType']),link_job])
                                print('====================')
                                record_count += 1
                                print('Crawled:', record_count , 'jobs')
                                now = datetime.datetime.now()
                                print('Time:', now.strftime("%Y-%m-%d %H:%M:%S"))
                                print('====================')
                        except Exception as e:
                            print("Error:",e)
                            continue
                except Exception as e:
                        print("Error:",e)
                        continue

