import sys
import time as t

def get_time():
    return t.strftime('%H:%M:%S')

# Mở file input
with open('./indeed_jobs_.txt', 'r') as f:
    lines = f.readlines()

unique_lines = []
duplicate_lines = []

# Loại bỏ các dòng trùng lặp và chỉ giữ lại một dòng duy nhất
for line in lines:
    if line not in unique_lines:
        unique_lines.append(line)
    else:
        duplicate_lines.append(line)

# Ghi dữ liệu đã được xử lý vào file output
output_file = 'output.csv'  # Thay đổi tên file output theo nhu cầu của bạn
with open(output_file, 'w') as f:
    for line in unique_lines:
        f.write(line)